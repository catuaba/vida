# Vida

Transformo a vida em um projeto de software livre.

Recebo contribuições ou pull requests, faço a avaliação de qual contribuição pode ser adicionada ou mergeada ao projeto vida.

Utilizo GNU GPLv3 para permitir que qualquer pessoa que ache essa vida interessante possa copiar, reproduzir, modificar ou compartilhar com quem quer que seja.
