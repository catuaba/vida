# Alimentação
  Um dia uma pessoa me disse que "aquela doença" chegou no território dela junto com a comida industrializada.
  Penso, nós pessoas, podemos criar um monte de coisa, criamos tanto que criamos toda uma industria que produz todo tipo de alimentos, feitos de coisas que nem nos damos conta do que é.
  Fazemos parte de um sistema, onde essa industria alimentícia é um dos motores do mercado. Pessoas comem comida... pessoas fabricam comida... pessoas compram comida... pessoas trabalham para fazer comida para que pessoas comprem, comam e assim todo mundo fica "feliz".
  Você já parou para avaliar o que usa para alimentar a tua maquininha, o teu hardware? Se comparássemos o nosso corpo com maquinas, a minha aqui tem um funcionamento, precisa de manutenção. É composta de peças, peças que vieram de uma fabrica humana.
  Imprecionante como essas peças se recriam sem que eu me dê conta, cada uma das minhas células morrem e nascem outras no lugar, é como se de certa forma eu trocasse de braço sem trocar o braço. Sabe, tem uma expressão que diz "trocar a roda com o carro em movimento", me sinto como esse carro, onde as células sozinhas fazem a troca da roda, nem percebo e já tem uma roda nova, ou seriam as células a roda em si?
  Enfim, para que essas células funcionem corretamente elas precisam de algumas coisas, entre elas o sono, luz do sol, movimentação e energia.
  A nossa energia vem direto da alimentação, o que colocamos na boca, viaja até o estomago e o resto do corpo, se transformará em combustível para o bom funcionamento do todo, desde a pequena célula até o meu próprio humor, então será que vale mesmo ficar colocando qualquer coisa na boca?
  Comecei a pensar, os pássaros fazem parte desse outro sistema que também fazemos parte, chamado Terra. Como eles sobrevivem sem cartão de crédito ou dinheiro? Como sabem o que podem comer ou não?
  Bixo gente é uma coisa engraçada, né? Precisa de uma loja para encontrar o que pode colocar na boca... Eu prefiro falar com pessoas, ler, ver com um monte de gente o que andam provando, fazer os meus próprios testes e pouco a pouco compreendendo o que me faz bem ou não.
  Escolher o que comer é um privilégio. Nem todo mundo pode escolher, para algumas pessoas falta dinheiro, para outras falta opção, para outras falta o conhecimento e para a maioria, o tempo. Um tempo que pode ajudar com que amplie essas opções.
  Como ainda fazemos parte desse sistema financeiro, se é para comprar algo e nós temos a possibilidade, o ideal seria comprar de pequenas produtoras e pequenas comerciantes.
  Ainda temos a linha do que existe por aí fora das lojas e que podemos comer, que vem de outra linha de produção, as plantas anarquistas. Na casa da minha avó tinha um quintal com um monte de coisa e eu me divertia dar uma banda por lá, procurar uma planta e transformar ela em salada, chá, refogado, recheio, petisco... Cada planta tinha um monte tipo de uso e nessa onda comecei a explorar os quintais da minha vida, tem muita comida por aí que não precisa de nem um vintem, sei que não vou conseguir substituir toda a minha alimentação de uma vez, mas começo devagar, um alimento por vez..

## Azedinha ou Lingua de vaca (Rumex acetosa)
  Folha com um gostinho azedo, virou salada nos ultimos dias e um dos motivos pelo qual comecei a comer:
    - Os insetos comem
    - É gostosa
  Essa planta comia em casa, no tempo de criança e encontrei as vezes na feira do Parque da Água Branca, em SP. Nunca tinha visto ela crescer, agora aleatóriamente encontrei ela no quintal onde estou atualmente, em Tacuarembo/Uy.
  Boa para salada, vira suco e cozida num molho bechamel parece apetitosa. Segundo a [Embrapa](https://ainfo.cnptia.embrapa.br/digital/bitstream/item/160999/1/folder-azedinha.pdf) tem um potencial antioxidante, rica em minerais como ferro, magnésio e potássio. Além de vitamina C

![azedinha](https://gitlab.com/catuaba/vida/-/raw/existencia/mais-que-palavras/lengua-de-vaca.jpeg)

## Beldroega (Portulaca oleracea)
  Plantinha suculenta, onde se come quase tudo (deixa a raiz de lado). A Rosa Moreira [diz](https://agriculturaemar.com/beldroegas-as-infestantes-que-tem-mais-valor-do-que-imagina/) que ela é fonte de ômega 3, vitaminas A, B e C e sais minerais. Betacaroteno... nome engraçado né. Ela tem isso também, por isso sua flor é amarela e junto com a cor, vem a vitamina A. Boa para o coração, sistema imunológico além de ser antioxidante, parece que com essa aí a gente não se enferruja XD
  Na [cartilha](https://www.ufrgs.br/viveiroscomunitarios/wp-content/uploads/2015/11/Cartilha-15.11-online.pdf) de PANCs[1] da IFRGS tem algumas informações sobre ela. A magia dessa planta é que ela nasce em todo tipo de condição, seja sol, sombra, terra com muita pedra... acho que se deixar muita cera na orelha, é capaz de nascer lá também.

![beldroega](https://gitlab.com/catuaba/vida/-/raw/existencia/mais-que-palavras/beldroega.jpg)

## Caruru (Amaranthus sp.)
  Essa é outra danada, se come folha e sementes (apesar que eu como talo também). Dela pode sair um refogado afogado em tudo de bom que tiver, cebola, alho e outras coisinhas além de se transformar em um bom recheio para tortas e quiches.
  Ela tá recheada de betacaroteno, vitamina C, ferro e potássio. As sementes possuem aminoacidos essenciais, são coisinhas que nosso corpo não pode sintetizar por conta própria mas precisa para um bom funcionamento, tem proteína na bichinha. Se transformar ela em farinha dá até para fazer pães e bolos. Chamam ela de pseudocereal, poderia [competir](https://dialnet.unirioja.es/descarga/articulo/6521553.pdf) lado a lado com o milho, arroz e o trigo.
![caruru](https://gitlab.com/catuaba/vida/-/raw/existencia/mais-que-palavras/caruru.jpeg)


## Neem (Azadirachta indica)
  Planta originaria da India, muito usada na medicina ayuvetica, a arvore livre da India (the free tree of India) Azadi em persa eh livre e diracht == arvore.
  Planta livre de insetos e doencas, uma planta dita "abencoada" pela grande potencialidades que tem, por exemplo, um estudo feito com as flohas listou 18 propriedades que eu preciso traduzir pq nao sei dizer tudo isso em portugues
  [Aqui](https://sci-hub.se/10.2174/1568011053174828) ta o documento em ingles que encontrei sobre o estudo da folha. Tudo isso comecou por causa do meu piolho! Primeiro, ontem uma colega tava contando como os galhos verdes do neem sao usados como escova de dente/pasta de dente, pois eh, 2 em 1! Ai hj de manha me dei conta que a coceira que tenho na cabeca nao eh cabelo sujo, sao piolhos mesmo hahaha Contando isso pro pessoal, primeiro com o meu ingles de buteco, explicando o que eu tinha uma amiga me pediu pra queimar oleo com canfora e cravo (oi!?) e tambekm adicionar naftalina na mistura (eim!?) hahaha ela pensou que eu tinha pequenos mosquitos em volta da cabeca, eu e o resto da galera ficamos na duvida do pq dessa mistura pra combater piolho (2 pessoas haviam entendido a minha explicacao de "pequenos animais na cabeca") ai depois de rir ela me deu um shampoo onde a sua composicao tem oleo de coco, oleo de neem e semente de fruta-do-conde (pinha) ou como o pessoal chama aqui, sitaphal. Ai falando mais sobre o neem, uma companheira disse que eu poderia aplicar uma mascara de folhas de neem no cabelo, basicamente amassar um chumasso de folhas, aplicar na cabeca e esperar. Ai ja sabendo que neem eh bom pro dente, contra piolho fui procurar pra que mais essa planta era boa e me assusti com a lista. Como outras plantas que sao boas pra um monte de coisa, o gosto eh amargo! Deve ser por isso que os piolhos morrem, ficam com fome por nao comer cabeca amarga hehe



1- PANCs, Plantas Anarquicas Notoriamente Comestíveis(s), são plantas que a galera esqueceu que são comestíveis só porque não se vende no mercado.

