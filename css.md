# Estilo

  Como você definiria o seu estilo?
  Desde criança sempre tive algo com o cabelo, pelo fato da sua cor chamava muita a atenção das outras pessoas, ao ponte de gente perguntar para a minha mãe qual a cor de tintura ela usava em mim o.O
  Por causa da cor do meu cabelo descobri que a minha origem era um pouco mais complicada que a simples resposta: filha da sua mãe com o seu pai. Tinha coisas não contadas que com o tempo fui descubrindo.
  Ele, o cabelo, tinha uma forma e corpo que eu não sabia bem como lidar, minha mãe tem o cabelo lisinho, então ela tão pouco sabia lidar com o dito. Quando criança, corte curto para facilitar a vida, conforme passa o tempo, eu vou aprendendo a me pentear, ela já não precisa mais fazer isso o que permite que meu cabelo cresça, mas com esse crescimento, o que fazer com ele.
  "Que bonita..." isso escutava quando alisava o dito.
  "..." isso escutava quando ele tava solto, vivendo a sua vida da forma que tinha.
  Adolescente tem uma coisa intensa, tá nas extremidades entre o "quero ser aceita" e "sou rebelde". Na linha do querer ser aceita, alisava meu cabelo com o ferro de passar roupa, dava trabalho, queimava a cabeça mas fazia o esforço para escutar o "que bonita...".
  Queria dreads desde criança, nunca soube o porque, talvez tenha visto alguém assim, mas quando comentei isso com alguém que nem lembro quem foi, o que essa pessoa disse me ecoa até hoje "Não... isso vai estragar o seu cabelo". Não queria estragar o cabelo, afinal, queria escutar o "que bonita..."
  Nisso de que bonita, usava parte do meu dia para deixar o cabelo da maneira que as pessoas queriam, pensava que isso era o que eu queria... O que eu queria era ser aceita, e esse cabelo era uma porta para isso.
  O fato da sua cor me marcava, era uma identidade. A cor era eu, eu era a cor. Se alguém dizia que não era ruiva, minha cara ficava da cor do cabelo e já tinha uma resposta ácida na ponta da lingua.
  Já cheguei a chorar em um momento, quando me cortaram o cabelo mais do que eu havia pedido.
  Com o tempo fui me dando conta que o cabelo, assim como a roupa e outras coisas que colocamos na gente, é um signo, uma marca, que nos conecta, que nos afasta, seria um tipo de "propaganda", algo que mostra para as outras pessoas (e para a gente mesmo) o que somos. Mas o que sou, é o que aparento? Ou, sou o que aparento ser? Aí termina o meu ser? Não tem mais coisa?
  Fui deixando o liso forçado porque não queria me forçar, queria me aceitar como era.
  Fui deixando a acidez quando não me identificavam como ruiva.
  Fui deixando de querer escutar o "que bonita...".
  Fui deixando de me montar da maneira que fosse identificavel para as outras pessoas, comecei a me recriar.
  Aceitei o cabelo que tinha, como o corpo que tinha, mas ainda faltava algo.
  Um dia um colombiano me disse "você precisa de uma rasta..." "sim, eu preciso dessa rasta...".
  Já fazia tempo e foi o seu tempo foi chegando.
  Uma, duas, três, quase toda a cabeça... Ainda faltava uma parte sem atar, sem fazer crochet na cabeça, já que eu tramo tanto, lã, codigo, arte, por que não tramar toda a cabeça? Estou presa no "que bonita..."?
  Pronto, fechei a cabeça.
  Cultivei as rastas, sempre tinha uma agulha a mão para colocar os fios onde deveriam estar.
  Passou o tempo abandonei a agulha, não preciso controlar esses fios, eles sabem ondem devem estar e se não querem se prender, que eu faço, os forço? Já não quero forçar as coisas, quero aceitar.
  Aí comecei a escutar "que bonita rastas..." Uai... essa gente não se decide o.O
  Queria ter rastas, tive... mas eu sou as rastas? Eu sou o cabelo? O que sou se limita ao que aparento?
  Para alguns povos indigenas, o cabelo é sua marca de identidade grupal além de orgulho individual, ao ponto de quando o indivídio era sujeitado à alguém, lhe cortam o cabelo como sinal de derrota. Nos campos de concentração, raspavam os cabelos das pessoas judias para remover sua identidade. Todos vestiam a mesma roupa, mesmo cabelo e seu nome era transformado em um mero número.
  Remover a identidade de alguém é um dos métodos de para facilitar a dominação e domesticação. Subjeção. Assim que a pessoa não se reconhece e não se identifica, fica mais fácil de controlar-la.
  Removi minha identidade. Cortei as rastas. Não sou as rastas. Estou nessa procura de quem sou e nessa procura percebi que não sou somente o que me vejo por fora, existe algo por dentro que quero escutar melhor.
  Como navegadora dos extremos que sou, queria raspar tudo de uma vez, sair do R ao R, do rasta ao raspada, mas também estou aprendendo a reduzir essas margens, não precisa ser 1 ou 0, apesar do binário ser parte da minha vida, o computador e o código abriram porteiras na minha vida, a vida é mais que 0 e 1, existem muitos mais nuances.
  Cortei, cortei baixinho e me diverti. Cortei do jeito que podia, com a tesoura que tinha e percebi que eu continuava ali, remover aquela moldura não removeu o meu ser, existe algo em mim que é mais amplo que esse recorte.
  Encontrei um maquina, agora posso caminhar mais um degrau, especialmente depois da provocação de uma amiga "ainda não consegui passar a 0", passei. O processo da "0", me ver no espelho durante o ato era como a cena de filme, onde alguém atacada pela insanidade, se desfaz daquilo que muitas pessoas são atadas, isso que a sociedade zela, o cabelo.
  Que peso tem o cabelo? Percebi que ele pesa de maneira distinta... se você quer ter e não o tem, seja por clavice, debilidade da saúde, estresse ou tratamento químico, te entristesse, busca peruca, busca remedio, busca tampar aquela pele no topo da cabeça. Agora quando se abraça essa pele, quando se escolhe por mostrá-la, o peso é bem distinto, é bem mais leve.
  Meu amigo de princípio não conseguiu me ajudar a raspar, ele tava mais preso no meu cabelo do que eu... mas enquanto eu seguia lá, no meu processo de "loucura" imposta, ele topou aceitar a aparar o que faltava atrás, e assim foi o cabelo. Ele se foi mas eu continuava ali. Sigo sendo eu, mas uma eu careca e agora sentindo o mundo com outra parte do meu corpo, o topo da cabeça.
